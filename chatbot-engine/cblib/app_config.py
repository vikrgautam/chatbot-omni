"""Load configuration from .yaml file."""
import confuse
import logging


logger = logging.getLogger(__name__)

class MyConfig(confuse.Configuration):
    def config_dir(self):
        return './apps-config/'

class BotConfig:
    config = MyConfig('Splitwise2Cfg', __name__)

    def __init__(self, env):
        #config = confuse.Configuration('MyApp', __name__)
        #self.config.set_dir('./apps-config/')
        logger.info("BotConfig::__init__()... Done  "+str(env))
        #self.config.set_file('./app-korkevin-config.yml')
        if env == 'prod':
            self.config.set_file('./apps-config/app-korkevin-cfg-prod.yml')
        elif env == 'dev':
            self.config.set_file('./apps-config/app-korkevin-cfg-dev.yml')
        else:
            self.config.set_file('./apps-config/app-korkevin-cfg-locl.yml')
        logger.debug("BotConfig::__init__()... Done  ")

    def get_config(self):
        self.config.dump()
        logger.debug("Config:: Dump:%s"%self.config)
        return self.config

    def get_key_val(self, root=None, subtree1=None, subtree2=None, subtree3=None):
        logger.debug("Config:: Keys="+str(root)+"|"+str(subtree1)+"|"+str(subtree2)+"|"+str(subtree3))
        #runtime = config['env']['app']['databases'].get()
        if subtree2 == None:
            if subtree1 == None:
                logger.debug("Config:: Val="+str(self.config[root].get()))
                return self.config[root].get()
            else:
                logger.debug("Config:: Val="+str(self.config[root][subtree1].get()))
                return self.config[root][subtree1].get()
        else:
            logger.debug("Config:: Val="+str(self.config[root][subtree1][subtree2].get()))
            return self.config[root][subtree1][subtree2].get()

if __name__ == "__main__":
    run_env = 'dev'
    botcfg = BotConfig(run_env)
    botcfg.get_config()
    #env = botcfg.get_key_val(root='env', subtree1='env_setp')
    logger.info("Env-Setup:: setup : %s"%run_env)
    logger.info(botcfg.get_key_val(root='apps', subtree1='korero_kevin', subtree2='bot_name'))
    logger.info(botcfg.get_key_val(root='databases', subtree1='postgre', subtree2='jdbcURL'))
    logger.info(botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='url_gen_otp'))
    logger.info(botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='url_verify_otp'))

