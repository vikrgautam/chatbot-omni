from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, DateTime, func, MetaData, Table, ForeignKey
from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.dialects import postgresql
from datetime import datetime
import psycopg2
import logging
import cblib.app_config as cfg

logger = logging.getLogger(__name__)
#logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)

Base = declarative_base()

class Users_Data(Base):
    __schema__ = 'chatbot'
    __tablename__ = 'tbl_bot_users'
    u_id = Column(Integer, primary_key=True, autoincrement=True)
    req_time = Column(Date, default=datetime.now())
    user_name_first = Column(String(32))
    user_name_last = Column(String(32))
    user_id = Column(String(32))
    user_desc = Column(String(32))
    user_ent_name = Column(String(32))
    user_profile_id = Column(Integer)
    user_designation = Column(String(32))

class Bot_Config_Data(Base):
    __schema__ = 'chatbot'
    __tablename__ = 'tbl_bot_config'
    bot_id = Column(Integer, primary_key=True, autoincrement=True)
    req_time = Column(Date, default=datetime.now())
    u_id = Column(Integer, ForeignKey("tbl_bot_users.u_id"), nullable=False)
    proj_name = Column(String(32))
    bot_name = Column(String(32))
    bot_avtar_name = Column(String(32))
    bot_category = Column(String(32))
    bot_image = Column(String(32))
    msg_welcome = Column(String(160))
    welcome_image = Column(String(32))
    welcome_image_loc = Column(String(32))
    msg_bye = Column(String(160))
    msg_failed = Column(String(160))
    msg_conn_err = Column(String(160))
    in_channel_rest = Column(String(240))
    in_channel_widget = Column(String(240))
    in_channel_webchat = Column(String(240))
    in_channel_whatsapp = Column(String(240))
    in_channel_fbmessanger = Column(String(240))

class Kevin_Visitor_Data(Base):
    __schema__ = 'chatbot'
    __tablename__ = 'tbl_kevin_visitors_data'
    id = Column(Integer, primary_key=True, autoincrement=True)
    bot_id = Column(Integer, ForeignKey("tbl_bot_config.bot_id"), nullable=False)
    enter_time = Column(Date, default=datetime.now())
    proj_name = Column(String(32))
    bot_name = Column(String(32))
    bot_avtar_name = Column(String(32))
    bot_category = Column(String(32))
    sender_id = Column(String(48))
    input_channel = Column(String(24))
    u_name = Column(String(56))
    u_emailid = Column(String(80))
    u_mobile = Column(String(16))
    otp_sms_generated = Column(String(32))
    otp_sms_2_verify = Column(String(8))
    is_otp_verified = Column(Integer)
    selected_offering = Column(String(80))
    selected_offering_sub = Column(String(16))
    status = Column(Integer)
    update_on = Column(Date, default=datetime.now())
    
    def __str__(self):
        return "<Kevin_Visitor_Data(id={}, enter_time='{}' proj_name='{}', bot_name='{}', bot_avtar_name={}, bot_category={}, sender_id='{}', input_channel='{}', u_name='{}', u_emailid='{}', u_mobile='{}', otp_sms_generated='{}', otp_sms_2_verify='{}', is_otp_verified={}, selected_offering='{}', selected_offering_sub='{}')>".format(self.id, self.enter_time, self.proj_name, self.bot_name, self.bot_avtar_name, self.bot_category, self.sender_id, self.input_channel, self.u_name, self.u_emailid, self.u_mobile, self.otp_sms_generated, self.otp_sms_2_verify, self.is_otp_verified, self.selected_offering, self.selected_offering_sub)

    def __repr__(self):
        return "<Kevin_Visitor_Data(id={})>".format(self.id)

class DbConfig():
    engine = None
    Session = None

    def __init__(self, env):
        logger.debug("DbConfig::__init__()... Started  ")
        botcfg = cfg.BotConfig(env)
        botcfg.get_key_val(root='databases', subtree1='postgre', subtree2='jdbcURL')

        #self.DB_URI = 'postgres+psycopg2://dev:CRUD_x_dev#321@10.254.254.82:5432/omni'
        self.DB_URI = botcfg.get_key_val(root='databases', subtree1='postgre', subtree2='jdbcURL')

        #Base.metadata.schema = "chatbot"
        logger.info("DbConfig::__init__()... Done  "+str(env))

    def connectDB(self):
        #logger.debug("DB:: "+str(self.DB_URI)+" Connecting...")
        self.engine = create_engine(self.DB_URI, pool_size=5, max_overflow=0, pool_pre_ping=True, pool_recycle=3600, echo=True)
        logger.info("DB:: "+str(self.DB_URI)+" Connected...")

        #self.Session = sessionmaker(bind=ac_engine, autoflush=False, expire_on_commit=False)
        self.Session = scoped_session(sessionmaker(bind=self.engine, autocommit=False, autoflush=False))
        logger.info("DB:: Session Bindeded.")

    def recreateDB(self):
        Base.metadata.create_all(engine)
        logger.info("DB:: Metadata re-created... Done")
#recreateDB()

    def open_session(self):
        if self.engine == None:
            self.connectDB()
        ssn = self.Session()
        logger.debug("DB:: Session Opened."+str(ssn))
        return ssn

    def close_session(self, ssn):
        if ssn != None:
            ssn.close()
        logger.debug("DB:: Session Closed."+str(ssn))

    def insert_data(self, u_data):
        rtrn = False
        logger.debug("DB:: Saving uData: "+str(u_data))
        ssn = self.open_session()
        ssn.execute("SET search_path TO chatbot")
        try:
            ssn.add(u_data)
            ssn.flush()
            rid = ssn.commit()
            logger.info("DB:: ret_id:"+str(rid))
    #        ssn.refresh(u_data)
    #        logger.debug(u_data.id)
            rtrn = True
        except Exception as error:
            logger.error("DB:: Saving uData..."+str(error))
        finally:
            ssn.close()
        return rtrn

    def insert_data_rid(self, u_data):
        rtrn = 0
        logger.debug("DB:: Saving uData: "+str(u_data))
        ssn = self.open_session()
        ssn.execute("SET search_path TO chatbot")
        try:
            ssn.add(u_data)
            ssn.flush()
            rid = ssn.commit()
            logger.debug("DB:: ret_id:"+str(rid))
            ssn.refresh(u_data)
            logger.info("DB:: returned visitor_id:"+str(u_data.id))
            rtrn = u_data.id
        except Exception as error:
            logger.debug("DB:: Saving uData..."+str(error))
        finally:
            ssn.close()
        return rtrn

    def merg_data(self, u_data):
        rtrn = False
        logger.debug("DB:: Updating uData: "+str(u_data))
        ssn = self.open_session()
        ssn.execute("SET search_path TO chatbot")
        try:
            ssn.merge(u_data)
            ssn.flush()
            rid = ssn.commit()
            logger.info("DB:: ret_id:"+str(rid))
            rtrn = True
        except Exception as error:
            logger.error("DB:: Updating uData..."+str(error))
        finally:
            ssn.close()
        return rtrn

    def load_kevin_u_data(self):
        logger.debug("DB:: Loading uData: ")
        ssn = self.open_session()
        ssn.execute("SET search_path TO chatbot")
        try:
            #rslt = s.query(Kevin_Visitor_Data).first()
            rslt = ssn.query(Kevin_Visitor_Data).all()
            logger.info("DB:: Loaded uData: "+str(rslt))
        except Exception:
            logger.error("DB: Loading user..."+str(error))
        finally:
            ssn.flush()
            ssn.close()
        return rslt

    def load_fltrd_kevin_u_data(self, b_id=None):
        rslt = None
        logger.debug("DB:: Loading Filtered: Bot config")
        ssn = self.open_session()
        ssn.execute("SET search_path TO chatbot")
        try:
            rslt = ssn.query(Bot_Config_Data).filter_by(bot_id=int(b_id)).first()
            logger.debug("filtered: "+str(rslt))
        except Exception as error:
            logger.error("Err: Loading user..."+str(error))
        finally:
            ssn.flush()
            ssn.close()
        logger.info("DB:: Loaded Filtered: Bot Config "+str(rslt))
        return rslt

def test_ins_func_users():
    logger.debug("DB:: Saving user data:...")
    u_data = Users_Data(
        user_name_first = "Korero",
        user_name_last = "Kevin Bot",
        user_id = "dev_cb_user",
        user_desc = "Chatbot user of Korero platforms",
        user_ent_name = "Korero Platforms",
        user_profile_id = 1,
        user_designation = "developer"
    )
    logger.info("DB:: Saving user data:..."+str(u_data))
    resp = insert_data(u_data)
    logger.info("DB:: visitor data saved resp:%s"%resp)

def test_ins_func_config():
    logger.debug("DB:: Saving Bot Config data:...")
    u_data = Bot_Config_Data(
        u_id = 1,
        proj_name = "cbot_korero_kevin",
        bot_name = "Korero bot",
        bot_avtar_name = "Kevin",
        bot_category = "Lead Generation",
        bot_image = "",
        msg_welcome = "HI,Welcome to Korero Platform",
        welcome_image = "",
        welcome_image_loc = "Right",
        msg_bye = "Thank you. We'll reach out to you as soon as possible.",
        msg_failed = "One more time?",
        msg_conn_err = "Woof! It feels like cold winter. Please Retry Again ",
        in_channel_rest = "",
        in_channel_widget = "",
        in_channel_webchat = "",
        in_channel_whatsapp = "",
        in_channel_fbmessanger = ""
    )
    logger.info("DB:: Saving Bot Config data:..."+str(u_data))
    resp = insert_data(u_data)
    logger.info("DB:: Bot Config data saved resp:%s"%resp)

def test_ins_func_visitor():
    logger.debug("DB:: Saving vistor data:...")
    in_offering_chnnl_selctd = "SMS"
    in_offering_mau_selctd = '20-50K'
    u_data = Kevin_Visitor_Data(
        bot_id = 101,
        proj_name = "test_proj",
        bot_name = "dev_korero",
        bot_avtar_name = "dev_kevin",
        bot_category = "dev_lead_gen",
        sender_id = "23423-234n1ds43-32fd23",
        input_channel = "rest",
        u_name = "Test Name",
        u_emailid = "tn@gmail.com",
        u_mobile = "1111111111",
        otp_sms_generated = "TS27654753234342"
#        otp_sms_2_verify = "67462",
#        is_otp_verified = 1,
#        selected_offering = "CPaaS",
#        selected_offering_sub = in_offering_chnnl_selctd if in_offering_chnnl_selctd != '' else in_offering_mau_selctd
    )
    logger.info("DB:: Saving vistor data:..."+str(u_data))
#    resp = insert_data(u_data)
#    resp = insert_data_rid(u_data)
    resp = merge_data(u_data)
    logger.info("DB:: visitor data saved resp:%s"%resp)

def test_updt_func_visitor():
    logger.debug("DB:: Saving vistor data:...")
    in_offering_chnnl_selctd = "SMS"
    in_offering_mau_selctd = '20-50K'
    u_data = Kevin_Visitor_Data(
        id=8,
        otp_sms_2_verify = "67462",
        is_otp_verified = 1,
        selected_offering = "CPaaS",
        selected_offering_sub = in_offering_chnnl_selctd if in_offering_chnnl_selctd != '' else in_offering_mau_selctd
    )
    logger.info("DB:: Saving vistor data merged:..."+str(u_data))
    resp = merge_data(u_data)
    logger.info("DB:: visitor data merged resp:%s"%resp)

def test_ld_func():
    db_obj = DbConfig(run_env)
    logger.debug("DB:: Loading Test data:...")
    db_obj.load_kevin_u_data()
    logger.debug("DB:: Loading Test data-2:...")
    db_obj.load_fltrd_kevin_u_data(b_id='101')

if __name__ == "__main__":
    run_env = 'dev'
    db_obj = dbm.DbConfig(run_env)
    test_ld_func()
    #bot_config_obj = load_fltrd_kevin_u_data(b_id=101)
    #logger.debug("bot_config_obj= "+str(bot_config_obj.bot_id)+"|"+str(bot_config_obj.proj_name))
    #test_ins_func_users()
    #test_ins_func_config()
    #test_ins_func_visitor()
    #test_updt_func_visitor()


