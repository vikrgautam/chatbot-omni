import requests
import logging
import json
import cblib.app_config as cfg

logger = logging.getLogger(__name__)

class SmsPush():
    API_GENERATE_OTP = None
    API_VERIFY_OTP = None
    user_name = None
    user_pswd = None
    channel = None
    msg_template = None
    sender_id = None
    entity_id = None
    template_id = None

    def __init__(self, env):
        logger.debug("DbConfig::__init__()... Started  ")
        botcfg = cfg.BotConfig(env)
        botcfg.get_key_val(root='databases', subtree1='postgre', subtree2='jdbcURL')

        #API_GENERATE_OTP = "http://digiconnect.spicedigital.in/omni-twofa-api/api/v1/twoFA/generateOtp"
        #API_VERIFY_OTP = "http://digiconnect.spicedigital.in/omni-twofa-api/api/v1/twoFA/validateOtp"
        self.API_GENERATE_OTP = botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='url_gen_otp')
        self.API_VERIFY_OTP = botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='url_verify_otp')
        self.user_name = botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='user_name')
        self.user_pswd = botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='user_pswd')
        self.channel = botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='channel')
        self.msg_template = botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='msg_template')
        self.sender_id = botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='sender_id')
        self.entity_id = botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='entity_id')
        self.template_id = botcfg.get_key_val(root='apis', subtree1='sms_otp', subtree2='template_id')

    def call_url_generate_otp(self, mdn=None):
        data_gen={'channel':self.channel,'message':self.msg_template,'msisdn':mdn,'otpFlag':True,'password':self.user_pswd,'senderId':self.sender_id,'userName':self.user_name,'entityId':self.entity_id,'templateId':self.template_id }
        logger.debug("Generate URL req start.... "+str(self.API_GENERATE_OTP))
        #logger.debug("Generate URL POST data.... "+str(self.data_gen))
        logger.debug("Generate URL POST Json.... "+str(json.dumps(data_gen)))
        #resp = requests.post(url = self.API_GENERATE_OTP, data = data_gen)
        resp = requests.post(url = self.API_GENERATE_OTP, headers={'Accept': '*/*', 'Content-Type': 'application/json'}, data = json.dumps(data_gen))
        req_url = resp.text
        logger.debug("Generate-API Resp:%s"%req_url)
        #resp_j = json.loads(resp.text)
        #logger.debug("RespCode : %s"%resp_j.get("response"))
        #resp_j1 = resp_j.get("response")
        #logger.debug("RespCode : %s"%resp_j1[0].get("code"))
        return resp.text

    def call_url_validate_otp(self, mdn=None, uin_otp=None):
        data_vald={'channel':self.channel,'msisdn':mdn,'otp':uin_otp,'userName':self.user_name}
        logger.debug("Validate URL req start.... "+str(self.API_VERIFY_OTP))
        #logger.debug("Validate URL POST data.... "+str(data_vald))
        logger.debug("Validate URL POST Json.... "+str(json.dumps(data_vald)))
        #resp = requests.post(url = self.API_VERIFY_OTP, data = data_vald)
        resp = requests.post(url = self.API_VERIFY_OTP, headers={'Accept': '*/*', 'Content-Type': 'application/json'}, data = json.dumps(data_vald))
        req_url = resp.text
        logger.debug("The Validate URL resp:%s"%req_url)
        #resp_j = json.loads(resp.text)
        #logger.debug("RespCode : %s"%resp_j.get("response"))
        #resp_j1 = resp_j.get("response")
        #logger.debug("RespCode : %s"%resp_j1[0].get("code"))
        return resp.text

if __name__ == "__main__":
    run_env = 'dev'
    sms_obj = SmsPush(run_env)
    #call_url_generate_otp("8628887477")
    #call_url_validate_otp("8628887477", "23072")


