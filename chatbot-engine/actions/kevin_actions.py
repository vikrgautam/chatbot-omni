# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
from rasa_sdk.forms import FormAction
import re
import json
import logging
import cblib.lib_https as lib_http
import cblib.db_models as dbm
import cblib.app_config as cfg
from datetime import datetime

run_env = 'prod'

logger = logging.getLogger(__name__)
db_obj = dbm.DbConfig(run_env)
sms_obj = lib_http.SmsPush(run_env)

class ValidateVisitorDataForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_visitor_data_form"

    def validate_slot_uname(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> Dict[Text, Any]:
        """Validate visitor name value."""

        if len(slot_value) > 1 and re.search(r'\w{2,56}',slot_value):
            # validation succeeded, set the value of the "cuisine" slot to value
            logger.info("# visitor_nm validation succeeded... OK")
            return {"slot_uname": slot_value}
        else:
            # validation failed, set this slot to None so that the user will be asked for the slot again
            logger.info("# visitor_nm validation failed... ")
            #dispatcher.utter_message(template="utter_ask_valid_slot_uname")
            return {"slot_uname": None}

    def validate_slot_uemail_id(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> Dict[Text, Any]:
        """Validate email value."""

        if re.search(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", slot_value):
            # validation succeeded, set the value of the "cuisine" slot to value
            logger.info("# email validation succeeded... OK")
            return {"slot_uemail_id": slot_value}
        else:
            # validation failed, set this slot to None so that the user will be asked for the slot again
            logger.info("# email validation failed... ")
            #dispatcher.utter_message(template="utter_ask_valid_slot_uemail_id")
            return {"slot_uemail_id": None}

    def validate_slot_umobile_no(self, slot_value: Any, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> Dict[Text, Any]:
        """Validate visitor mobile no value."""

        if len(slot_value) >= 9 and len(slot_value) <= 15 and re.search(r'\d{9,16}',slot_value):
            # validation succeeded, set the value of the "slot_umobile_no" slot to value
            logger.info("# mobile_no validation succeeded... OK")
            return {"slot_umobile_no": slot_value}
        else:
            # validation failed, set this slot to None so that the user will be asked for the slot again
            logger.info("# mobile_no validation failed... ")
            #dispatcher.utter_message(template="utter_ask_valid_slot_umobile_no")
            return {"slot_umobile_no": None}

class VisitorDataForm(Action):
    def name(self) -> Text:
        logger.debug("visitor_data_form __init__: Done")
        return "submit_visitor_data_form"

#    def submit(self, dispatcher, tracker, domain):
    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        logger.debug("Submit visitor form data... : ")
        conversation_id=tracker.sender_id
        in_channel = tracker.get_latest_input_channel()
        logger.debug("Saving_visitor:: conversation_id... : %s"%str(conversation_id))
        in_uname = tracker.get_slot("slot_uname")
        in_uemailid = tracker.get_slot("slot_uemail_id")
        in_umobno = tracker.get_slot("slot_umobile_no")

        appcfg = cfg.BotConfig(run_env)
        #env = appcfg.get_key_val(root='env', subtree1='env_setp')
        app_id = appcfg.get_key_val(root='apps', subtree1='korero_kevin', subtree2='app_id')
        logger.info("Saving_visitor:: converse_id: "+str(conversation_id)+", Slot-mobno: "+str(in_umobno))

        botcfg = db_obj.load_fltrd_kevin_u_data(b_id=app_id)
        logger.debug("Saving_visitor:: Bot Config DB loaded... "+str(botcfg))
        proj_nm = botcfg.proj_name
        bot_nm = botcfg.bot_name
        bavtar_nm = botcfg.bot_avtar_name
        bcatgry_nm = botcfg.bot_category
        u_data = dbm.Kevin_Visitor_Data(
            bot_id = app_id,
            enter_time = datetime.now(),
            proj_name = proj_nm,
            bot_name = bot_nm,
            bot_avtar_name = bavtar_nm,
            bot_category = bcatgry_nm,
            sender_id = conversation_id,
            input_channel = in_channel,
            u_name = in_uname,
            u_emailid = in_uemailid,
            u_mobile = in_umobno,
            status = 0,
            update_on = datetime.now()
        )
        logger.debug("DB:: visitor data saving:%s"%u_data)
        rid = db_obj.insert_data_rid(u_data)
        logger.info("DB:: visitor data saved VId: "+str(rid))
        tracker.slots['v_id'] = rid
        logger.debug("Slots:: "+str(tracker.slots))
        return [SlotSet('v_id', rid)]

class ActionGenerateOTP(Action):
# This method should send user data collected in form to backend. So that backend could 
# generate and send an OTP to registered mobile number.
    def name(self) -> Text:
        logger.debug("action_generate_otp __init__: Done")
        return "action_generate_otp"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        logger.debug("GeneratingOTP... : ")
        conversation_id=tracker.sender_id
        v_id = tracker.get_slot('v_id')
        logger.debug("GeneratingOTP:: conversation_id... : "+str(conversation_id)+" VId: "+str(v_id))
        slot_umob_no=tracker.get_slot('slot_umobile_no')

        resp = sms_obj.call_url_generate_otp(mdn=slot_umob_no)
        logger.debug("API:: generate resp:%s"%resp)
        resp_j = ""
        resp_j1 = ''
        try:
            resp_j = json.loads(resp)
        except Exception as error:
            resp_j = resp
            logger.warn("API:: response type not JSon... "+str(error))
        #logger.debug("Response : %s"%resp_j.get("response"))
        if ("response" in resp_j):
            try:
                resp_j1 = resp_j.get("response")[0]
            except Exception as error:
                resp_j1 = resp_j.get("response")
                logger.error("API:: Saving uData..."+str(error))
        else:
            resp_j1 = resp_j
        #logger.debug("Response-X : %s"%resp_j.get("response"))
        logger.debug("API:: RespAttr : "+str("code" in resp_j1))
        respcd_suc = {'0'}
        resp_cd = -1
        is_genrtd = 0
        if ("code" in resp_j1):
            logger.info("API:: RespCode: %s"%resp_j1.get("code"))
            resp_cd = resp_j1.get("code")
        elif ("error" in resp_j1):
            logger.info("API:: RespCode-Err: %s"%resp_j1.get("error").get("code"))
            resp_cd = resp_j1.get("error").get("code")
        else:
            logger.info("API:: RespCode-Invalid: -11")
            resp_cd = -11
        logger.debug("API:: RespStatus : "+str((resp_cd in respcd_suc)))
        otp_genid = ''
        if (resp_cd in respcd_suc):
            is_genrtd = 1
            otp_genid = resp_j1.get("messageId")
            logger.debug("API:: messageId for slot... : "+str(otp_genid))
            dispatcher.utter_message(template="utter_otp_generated_sent")
            dispatcher.utter_message(template="utter_enter_otp")
        else:
            dispatcher.utter_message(template="utter_fail_scenario")

        u_data = dbm.Kevin_Visitor_Data(
            id=v_id,
            otp_sms_generated = otp_genid,
            status = resp_cd,
            update_on = datetime.now()
        )
        logger.debug("DB:: visitor msgid saving:%s"%u_data)
        rid = db_obj.merg_data(u_data)
        logger.info("DB:: visitor msgid saving resp: "+str(rid)+", msgId"+otp_genid)
        tracker.slots['otp_generatdid'] = otp_genid
        logger.debug("Slots:: "+str(tracker.slots))
        return [SlotSet('otp_generatdid', otp_genid)]
#        return []

class ActionVerifyOTP(Action):
# This method should send otp received by user to backend. So that backend could 
# generate and send an OTP to registered mobile number.

    def name(self) -> Text:
        logger.debug("action_verify_otp __init__: Done")
        return "action_verify_otp"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        logger.debug("VerifyingOTP... : %s"%str(tracker.get_slot('otp_generatdid')))
        conversation_id=tracker.sender_id
        logger.debug("conversation_id... : %s"%str(conversation_id))
        v_id = tracker.get_slot('v_id')
        slot_umob_no=tracker.get_slot('slot_umobile_no')
        otp_recvd = tracker.latest_message['text']
        logger.debug("otp received... : %s"%str(otp_recvd))
        logger.info("VerifyingOTP:: converse_id: "+str(conversation_id)+", visitor_id: "+str(v_id))
        resp = sms_obj.call_url_validate_otp(mdn=slot_umob_no, uin_otp=otp_recvd)
        logger.debug("API:: verify resp:%s"%resp)

        resp_j = ""
        resp_j1 = ''
        try:
            resp_j = json.loads(resp)
        except Exception as error:
            resp_j = resp
            logger.warn("API:: response type not JSon... "+str(error))
        #logger.debug("Response : %s"%resp_j.get("response"))
        if ("response" in resp_j):
            try:
                resp_j1 = resp_j.get("response")[0]
            except Exception as error:
                resp_j1 = resp_j.get("response")
                logger.error("API:: Saving uData..."+str(error))
        else:
            resp_j1 = resp_j
        #logger.debug("Response-X : %s"%resp_j.get("response"))
        logger.debug("API:: RespAttr : "+str("code" in resp_j1))
        respcd_suc = {'0'}
        resp_cd = -1
        is_verfd = 0
        if ("code" in resp_j1):
            logger.info("API:: RespCode: %s"%resp_j1.get("code"))
            resp_cd = resp_j1.get("code")
        elif ("error" in resp_j1):
            logger.info("API:: RespCode-Err: %s"%resp_j1.get("error").get("code"))
            resp_cd = resp_j1.get("error").get("code")
        else:
            logger.info("API:: RespCode-Invalid: -11")
            resp_cd = -11
        logger.debug("API:: RespStatus : "+str((resp_cd in respcd_suc)))
        if (resp_cd in respcd_suc):
            is_verfd = 1
            dispatcher.utter_message(template="utter_bot_offerings")
        else:
            dispatcher.utter_message(template="utter_fail_scenario")

        u_data = dbm.Kevin_Visitor_Data(
            id=v_id,
            otp_sms_2_verify = otp_recvd,
            is_otp_verified = is_verfd,
            status = resp_cd,
            update_on = datetime.now()
        )
        if v_id == None or v_id == '' :
            logger.info("No Visitor_id found in DB. Not uodating record...")
        else:
            resp = db_obj.merg_data(u_data)
        logger.info("DB:: visitor data verifyOTP-1 DBresp:%s"%resp)
        tracker.slots['otp_is_verifd'] = is_verfd
        tracker.slots['otp_2verify'] = otp_recvd
        logger.debug("Slots:: "+str(tracker.slots))
        if (is_verfd == 1):
            return [SlotSet('otp_is_verifd', is_verfd), SlotSet('otp_2verify', otp_recvd)]
        else:
            return [SlotSet('otp_is_verifd', is_verfd), SlotSet('otp_2verify', otp_recvd)]

class ActionSaveUData(Action):
# This method should save user collected data into PostGRE for reportings purpose

    def name(self) -> Text:
        logger.debug("action_save_udata __init__: Done")
        return "action_save_udata"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        logger.debug("Saving visitor Data-2... ")
        #logger.debug("Slots:: "+str(tracker.slots))
        conversation_id=tracker.sender_id
        v_id = tracker.get_slot('v_id')
        in_offering_selctd = tracker.get_slot("offering_selctd")
        in_offering_sub_selctd = tracker.get_slot("offering_sub_selctd")
        logger.info("Saving_uData:: converse_id: "+str(conversation_id)+", visitor_id: "+str(v_id)+", offering_selctd: "+str(in_offering_selctd)+", offering_sub_selctd: "+str(in_offering_sub_selctd))

        u_data = dbm.Kevin_Visitor_Data(
            id=v_id,
            selected_offering = in_offering_selctd,
            selected_offering_sub = in_offering_sub_selctd,
            status = 0,
            update_on = datetime.now()
        )
        logger.debug("DB:: visitor data last saving:%s"%u_data)
        if v_id == None or v_id == '' :
            logger.info("No Visitor_id found in DB. Not uodating record...")
        else:
            resp = db_obj.merg_data(u_data)
            logger.info("DB:: visitor data merged-2 resp:%s"%resp)
        return []

