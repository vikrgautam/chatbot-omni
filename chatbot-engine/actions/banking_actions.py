# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

from rasa_sdk.events import SlotSet
from rasa_sdk.forms import FormAction
import requests


class ActionFacilitySearch(Action):

    def name(self) -> Text:
        return "action_facility_search"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        print("====Inside ActionFacilitySearch====")
        facility = tracker.get_slot("facility_type")
        loc = tracker.get_slot("location")
        if facility == "branch":
            url = "https://www,hdfcbank.com/branch-atm-locator"
        else:
            url = "https://v1.hdfcbank.com/branch-atm-locator"
        print("Searching bank {} in {} location".format(facility, loc))
        url_ggl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+loc+"&types="+facility+"&radius=5000&key="
        url_ggl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query="+facility+"+"+loc+"&radius=5000&key=AIzaSyB_3H3-hJzkoXZmQ85F5mbuZi7c1mQ0gkI"
        print(url_ggl)
        x = requests.get(url_ggl)
        print(x.status_code)
        print(x.content)
        #dispatcher.utter_message(text="Sure! search {} at {} location using following link...".format(facility, loc))
        dispatcher.utter_message(template="utter_searched_location", facility_type=facility, location=loc, url=url)
        return [SlotSet("location", loc)]

class ActionSearchFacility(Action):
    """
    Search the Bank branch/ATM using location & cuisine.
    Required Parameters: Location, Cuisine
    """
    def name(self) -> Text:
        return "action_search_facility"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        print("====Inside ActionSearchFacility====")
        url_ggl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+loc+"&types="+facility+"&radius=5000&key="
        url_ggl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query="+facility+"+"+loc+"&radius=5000&key=AIzaSyB_3H3-hJzkoXZmQ85F5mbuZi7c1mQ0gkI"
        x = requests.get('https://w3schools.com')
        print(x.status_code)

        ## extract the required slots
        facility = tracker.get_slot("facility_type")
        location=tracker.get_slot("location")
        lat=tracker.get_slot("latitude")
        lon=tracker.get_slot("longitude")
        entity_id=tracker.get_slot("location_id")
        entity_type=tracker.get_slot("location_type")
        city_id=tracker.get_slot("city_id")

        ## extract the entities
        locationEntity=next(tracker.get_latest_entity_values("location"), None)
        user_locationEntity=next(tracker.get_latest_entity_values("user_location"), None)
        latEntity=next(tracker.get_latest_entity_values("latitude"), None)
        lonEntity=next(tracker.get_latest_entity_values("longitude"), None)

        ## if we latitude & longitude entities are found, set it to slot
        if(latEntity and lonEntity):
            lat=latEntity
            lon=lonEntity
        
        ## if user wants to search bank facility in his current location
        if(user_locationEntity or (latEntity and lonEntity) ):
            ##check if we already have the user location coordinates stoed in slots
            if(lat==None and lon==None):
                dispatcher.utter_message(text="Sure, please allow me to access your location 🧐",json_message={"payload":"location"})
                
                return []
            else:
                locationEntities=zomatoApi.getLocationDetailsbyCoordinates(lat,lon)
                location=locationEntities["title"]
                city_id=locationEntities["city_id"]
                entity_id=locationEntities["entity_id"]
                entity_type=locationEntities["entity_type"]
                
                ## store the user provided details to slot
                SlotSet("location", locationEntities["title"])
                SlotSet("city_id", locationEntities["city_id"])
                SlotSet("location_id", locationEntities["entity_id"])
                SlotSet("location_type", locationEntities["entity_type"])

        ## if user wants to search bank facility by location name
        if(locationEntity):
            locationEntities=zomatoApi.getLocationDetailsbyName(locationEntity)
            if(locationEntities["restaurants_available"]=="no"):
                dispatcher.utter_message("Sorry I couldn't find any {}  😓".format(facility))
                return []
            entity_id=locationEntities["entity_id"]
            entity_type=locationEntities["entity_type"]
            city_id=locationEntities["city_id"]
            SlotSet("location", locationEntities["title"])
        
        print("Entities:  ",entity_id," ",entity_type," ",cuisine_id," ",location," ",cuisine)
        print()

