import time
from flask import Flask, jsonify, request

app = Flask(__name__)


@app.route("/")
def hello():
    return "hello, bots"


"""
stores senders and state of chat
{
    [sender_id]: {
        paused: True | False,
        replies: [],
    }
}
"""
sender_store = {"123": {"paused": True, "replies": ["first reply"]}}


@app.route("/handoff/<sender_id>", methods=["GET", "POST"])
def handoff(sender_id):
    print("handdoff in progress... " + sender_id)
    if request.method == "POST":
        """
        POST
        add sender id to sender store
        """
        if sender_id in sender_store:
            sender_store[sender_id]["replies"].append(request.get_json()["message"])
            return jsonify(sender_store[sender_id])
        else:
            return jsonify({"error": "sender_id not found"})
    else:
        """
        GET
        retry 10 times with 10 sec sleeps to fetch info
        """
        print("condition = "+str(sender_id not in sender_store))
        if sender_id not in sender_store:
            sender_store[sender_id] = {"paused": True, "replies": ["/unpausex"]}

        ## poll on replies to fetch the message
        print("replies = "+str(sender_store[sender_id]["replies"]))
        while len(sender_store[sender_id]["replies"]) == 0:
            print("inside while")
            time.sleep(1)
        print("outside while")

        message = sender_store[sender_id]["replies"].pop(0)
        return jsonify({"message": message})


if __name__ == "__main__":
    app.run(host=app.config.get("HOST"), port=app.config.get("PORT"))

