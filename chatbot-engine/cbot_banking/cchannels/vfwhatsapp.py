import logging
from typing import Text, Dict, Optional, Callable, Awaitable, Any

from sanic import Blueprint, response
from sanic.request import Request

from rasa.core.channels.channel import (
    CollectingOutputChannel,
    UserMessage,
    InputChannel,
)
from rasa.core.channels.rest import RestInput
from rasa.utils.endpoints import EndpointConfig, ClientResponseError
from sanic.response import HTTPResponse
import json

logger = logging.getLogger(__name__)

def is_json_key_available(json, key):
    try:
        buf = json[key]
    except KeyError:
        return False
    return True

class VFWhatsAppOutput(CollectingOutputChannel):
    """A Value-first WhatsApp Callback communication channel"""
    
    @classmethod
    def name(cls) -> Text:
        return "vfwhatsapp"

    def __init__(self, endpoint: EndpointConfig) -> None:
        #logger.debug("Out:__init__:: endpoint.basic_auth="+str(endpoint.basic_auth))
        #logger.debug("Out:__init__:: endpoint.kwargs="+str(endpoint.kwargs["auth"]))
        self.callback_endpoint = endpoint
        super().__init__()

    async def _persist_message(self, message: Dict[Text, Any]) -> None:
        await super()._persist_message(message)

        try:
            #headers = {"Authorization": "Basic dXNlcm5hbWU6cGFzc3dvcmQ="}
            headers = {"Authorization": self.callback_endpoint.kwargs["auth"]}
            logger.debug("Out:_persist_message:: url="+str(self.callback_endpoint.url))
            logger.debug("Out:_persist_message:: headers="+str(headers))
            #logger.debug("Out:_persist_message:: headers="+str(self.callback_endpoint.headers))
            #logger.debug("Out:_persist_message:: basic_auth="+str(self.callback_endpoint.basic_auth))
            #logger.debug("Out:_persist_message:: message="+str(message))
            logger.info("Out:Req:: url="+str(self.callback_endpoint.url)+" headers="+str(headers))
            
            msg_str = ""+json.dumps(message, indent=1, separators=(", ", ": "),sort_keys=False)
            #print("Out:_persist_message:: msg_str="+msg_str)
            msg_json = json.loads(msg_str, strict=False)
            #print("Out:_persist_message:: msg_json="+str(msg_json))
            resp_payld = "{"
            resp_payld = resp_payld+"\"message\": \""+msg_json["text"]+"\" ,"
            resp_payld = resp_payld+"\"messageType\": 2 ,"
            resp_payld = resp_payld+"\"timestamp\": \"1604321900\" ,"
            resp_payld = resp_payld+"\"mobile\": \""+msg_json["recipient_id"]+"\" ,"
            totl = 0
            if is_json_key_available(msg_json, 'buttons'):
                logger.debug("Out:_persist_message:: Adding Buttons...")
                resp_payld = resp_payld+"\"buttons\": ["
            if is_json_key_available(msg_json, 'buttons'):
                for bttn in msg_json['buttons']:
                    logger.debug("Out:_persist_message:: Adding Button..."+bttn["title"])
                    resp_payld = resp_payld+"{\"title\": \""+bttn["title"]+"\", "
                    resp_payld = resp_payld+"\"payload\": \""+bttn["payload"]+"\"}"
                    totl = totl+1
                    if len(msg_json['buttons']) != totl:
                        resp_payld = resp_payld+", "
            if is_json_key_available(msg_json, 'buttons'):
                resp_payld = resp_payld+"], "
            resp_payld = resp_payld+"\"clientId\": "+str(self.callback_endpoint.kwargs["clientid"])
            resp_payld = resp_payld+"}"
            logger.info("Out:Req:: payload="+resp_payld)
            resp_json = json.loads(resp_payld, strict=False)
            vf_api_resp = await self.callback_endpoint.request(
                "post", content_type="application/json", headers=headers, json=resp_json
            )
            logger.info("Out:Resp:: vf_api_resp="+str(vf_api_resp))
        except ClientResponseError as e:
            logger.error(
                "Failed to send output message to callback. "
                "Status: {} Response: {} "
                "".format(e.status, e.text)
            )


class VfWhatsAppInput(RestInput):
    """A custom REST http alue-first WhatsApp input channel that responds using a callback server.

    Incoming messages are received through a REST interface. Responses
    are sent asynchronously by calling a configured external REST endpoint."""

    @classmethod
    def name(cls) -> Text:
        return "vfwhatsapp"

    @classmethod
    def from_credentials(cls, credentials: Optional[Dict[Text, Any]]) -> InputChannel:
        token = credentials.get("auth")
        logger.debug("from_credentials:: token="+str(token))
        return cls(EndpointConfig.from_dict(credentials))

    def __init__(self, endpoint: EndpointConfig) -> None:
        self.callback_endpoint = endpoint

    async def _extract_sender(self, req: Request) -> Optional[Text]:
        return req.json.get("from", None)

    # noinspection PyMethodMayBeStatic
    def _extract_message(self, req: Request) -> Optional[Text]:
        return req.json.get("text", None)

    def _extract_to(self, req: Request) -> Optional[Text]:
        return req.json.get("to", None)

    def _extract_content_type(self, req: Request) -> Optional[Text]:
        return req.json.get("content_type", None)

    def _extract_media_type(self, req: Request) -> Optional[Text]:
        return req.json.get("media_type", None)

    def _extract_media_data(self, req: Request) -> Optional[Text]:
        return req.json.get("media_data", None)

    def _extract_latitude(self, req: Request) -> Optional[Text]:
        return req.json.get("latitude", None)

    def _extract_longitude(self, req: Request) -> Optional[Text]:
        return req.json.get("longitude", None)

    def blueprint(
        self, on_new_message: Callable[[UserMessage], Awaitable[Any]]
    ) -> Blueprint:
        callback_webhook = Blueprint("callback_webhook", __name__)

        @callback_webhook.route("/", methods=["GET"])
        async def health(_: Request):
            return response.json({"status": "ok"})

        @callback_webhook.route("/webhook", methods=["POST"])
        async def webhook(request: Request) -> HTTPResponse:
            logger.debug("webhook:: request.body="+str(request.body))
            request.body.decode("utf-8")
            logger.info("webhook:: request.body.decode="+str(request.body))
            #req_str = ""+json.dumps((str(request.body.decode("utf-8"))).replace(',\\n    "',', "').replace('\\n    "','"').replace('\\n',''), indent=1, separators=(", ", ": "),sort_keys=False)
            #print("webhook:: req_str="+req_str)
            #print("webhook:: req_str.type="+str(type(req_str)))
            #req_json = json.loads(req_str)
            #print("webhook:: req_json="+str(req_json))
            #sender_id = req_json[0]
            #print("webhook:: req_json.from="+str(sender_id))
            sender_id = await self._extract_sender(request)
            text = self._extract_message(request)
            to = self._extract_to(request)
            content_type = self._extract_content_type(request)
            media_type = self._extract_media_type(request)
            media_data = self._extract_to(request)
            latitude = self._extract_latitude(request)
            longitude = self._extract_to(request)
            logger.debug("webhook:: from="+str(sender_id))
            logger.debug("webhook:: text="+str(text))
            logger.debug("webhook:: to="+str(to))
            logger.debug("webhook:: content_type="+str(content_type))
            logger.debug("webhook:: media_type="+str(media_type))
            logger.debug("webhook:: media_data="+str(media_data))
            logger.debug("webhook:: latitude="+str(latitude))
            logger.debug("webhook:: longitude="+str(longitude))

            collector = self.get_output_channel()
            logger.debug("webhook:: collector="+str(collector))
            await on_new_message(
                UserMessage(text, collector, sender_id, input_channel=self.name())
            )
            return response.text("success")

        return callback_webhook

    def get_output_channel(self) -> CollectingOutputChannel:
        return VFWhatsAppOutput(self.callback_endpoint)
