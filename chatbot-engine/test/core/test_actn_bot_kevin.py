from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, DateTime, func, MetaData, Table, ForeignKey
from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.dialects import postgresql
from datetime import datetime
#import psycopg2
import logging
import sys
sys.path.append('/home/gurjeet/git_5/chatbot-omni/chatbot-engine')
print("syspath="+str(sys.path))

import cblib.app_config as cfg
import cblib.db_models as dbm
run_env = 'dev'

logger = logging.getLogger(__name__)

def test_insert_visitor_data(conversation_id, in_uname, in_uemailid, in_umobno, in_channel):
    print("Test method called - test_insert_visitor_data")
    appcfg = cfg.BotConfig(run_env)
    app_id = appcfg.get_key_val(root='apps', subtree1='korero_kevin', subtree2='app_id')
    botcfg = dbm.DbConfig(run_env).load_fltrd_kevin_u_data(b_id=app_id)
    print("Bot Config Loaded... " +str(botcfg))

    proj_nm = botcfg.proj_name
    bot_nm = botcfg.bot_name
    bavtar_nm = botcfg.bot_avtar_name
    bcatgry_nm = botcfg.bot_category
    u_data = dbm.Kevin_Visitor_Data(
        bot_id = app_id,
        proj_name = proj_nm,
        bot_name = bot_nm,
        bot_avtar_name = bavtar_nm,
        bot_category = bcatgry_nm,
        sender_id = conversation_id,
        input_channel = in_channel,
        u_name = in_uname,
        u_emailid = in_uemailid,
        u_mobile = in_umobno,
        status = 0
    )
    print("Inserting vistor data..." +str(u_data))
    rid = dbm.DbConfig(run_env).insert_data_rid(u_data)
    print("visitor data saved. Record Id = " +str(rid))
    print("++++++++++++++++++++++++++++++++++++++++")

def test_update_otp_generated(r_id, otp, status):
    print("Test method called - test_update_otp_generated")
    u_data = dbm.Kevin_Visitor_Data(
        id = r_id,
        otp_sms_generated = otp,
        status = status
    )
    print("Inserting vistor data, otp generated..." + str(u_data))
    resp = dbm.DbConfig(run_env).merg_data(u_data)
    print("visitor data saved.. Response = " +str(resp))
    print("++++++++++++++++++++++++++++++++++++++++")

def test_update_otp_to_verify(r_id, otp_to_verify, is_verified, status):
    print("test method called - test_update_otp_to_verify")
    u_data = dbm.Kevin_Visitor_Data(
        id = r_id,
        otp_sms_2_verify = otp_to_verify,
        is_otp_verified = is_verified,
        status = status
    )
    print("Inserting vistor data, otp to verify..." + str(u_data))
    resp = dbm.DbConfig(run_env).merg_data(u_data)
    print("visitor data saved.. Response = " +str(resp))
    print("++++++++++++++++++++++++++++++++++++++++")

def test_update_offerings(r_id, in_offering_selctd, in_sub_offering_selctd, status):
    print("test method called - test_update_offerings")
    u_data = dbm.Kevin_Visitor_Data(
        id = r_id,
        selected_offering = in_offering_selctd,
        selected_offering_sub = in_sub_offering_selctd,
        status = status
    )
    print("Inserting vistor data, offerings..." + str(u_data))
    resp = dbm.DbConfig(run_env).merg_data(u_data)
    print("visitor data saved.. Response = " +str(resp))
    print("++++++++++++++++++++++++++++++++++++++++")

if __name__ == "__main__":
    r_id = test_insert_visitor_data("23423-234n1ds43-32fd23", "test_name", "test@test.com", 9023430434, "rest")
    test_update_otp_generated(r_id, 62620, 1)
    test_update_otp_to_verify(r_id, 62620, 1, 1)
    test_update_offerings(r_id, "kosync_a_cpaas_platform", "kosync_channel_whatsapp", 1)