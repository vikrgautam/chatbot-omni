# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionProcessUserDataAndGenerateOTP(Action):
# This method should send user data collected in form to backend. So that backend could 
# generate and send an OTP to registered mobile number.
    def name(self) -> Text:
        return "action_process_userdata_and_generate_otp"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="OTP sent to registered mobile number.")
        print(tracker.slots)

        return []

class ActionProcessOTPAndDoNeedful(Action):
# This method should send otp received by user to backend. So that backend could 
# generate and send an OTP to registered mobile number.

    def name(self) -> Text:
        return "action_process_otp_and_do_needful"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="Request ID is created and shared.")
        dispatcher.utter_message(text="Salary slip / Reimbursement form is shared.")

        return []
