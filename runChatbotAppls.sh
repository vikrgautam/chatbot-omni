#!/bin/bash

##  ./runChatbotAppls.sh [start|stop|train] [0|1|2|3] [prod|dev|locl1|locl2]

##  Env setup
#source $HOME/.bash_profile
if [ $3 == 'prod' ] ; then
  source /home/chatbot/envs_chatbot_omni/env_cb_py386_rasa225/bin/activate
elif [ $3 == 'dev' ] ; then
  #source /opt/envs_chatbot_omni/env_cb_py386_rasa213/bin/activate
  source /opt/envs_chatbot_omni/env_cb_py386_rasa224/bin/activate
fi

CB_APP_PREFIX_SRVR_DEVEL=/home/apps
CB_APP_PREFIX_SRVR_PROD=/home/chatbot/apps
CB_APP_PREFIX_VG=/home/spice/Workspace/Python
CB_APP_PREFIX_GUR=/home/gurjeet/git_5

CB_LOGS_PATH=/var/logs/chatbot

if [ $3 == "prod" ]; then
  CB_APP_PREFIX=$CB_APP_PREFIX_SRVR_PROD
  CB_LOGS_PATH=/home/chatbot/logs/chatbot
elif [ $3 == 'dev' ] ; then
  CB_APP_PREFIX=$CB_APP_PREFIX_SRVR_DEVEL
elif [ $3 == 'locl1' ] ; then
  CB_APP_PREFIX=$CB_APP_PREFIX_VG
elif [ $3 == 'locl2' ] ; then
  CB_APP_PREFIX=$CB_APP_PREFIX_GUR
fi

CB_APP_HOME=$CB_APP_PREFIX/chatbot-omni/chatbot-engine
CB_APP_HOME_HR=$CB_APP_HOME/cbot_hr/
CB_APP_HOME_BANK=$CB_APP_HOME/cbot_banking/
CB_APP_HOME_KORERO_KEVIN=$CB_APP_HOME/cbot_korero_kevin/
CB_APP_HOME_POC_HUMAN_HANDOFF=$CB_APP_HOME/cbot_poc_human_handoff/

CB_APP_HOME_PROJ=$CB_APP_HOME_HR

CB_PORT=5005

INDX=1
echo $2
if [ $2 -eq 0 ] ; then
        INDX=0
	CB_APP_HOME_PROJ=$CB_APP_HOME
        CB_PORT=5055
elif [ $2 -eq 1 ] ; then
	CB_APP_HOME_PROJ=$CB_APP_HOME_HR
        CB_PORT=5005
elif [ $2 -eq 2 ] ; then
	CB_APP_HOME_PROJ=$CB_APP_HOME_BANK
        CB_PORT=6006
elif [ $2 -eq 3 ] ; then
	CB_APP_HOME_PROJ=$CB_APP_HOME_KORERO_KEVIN
        CB_PORT=6007
elif [ $2 -eq 4 ] ; then
	CB_APP_HOME_PROJ=$CB_APP_HOME_POC_HUMAN_HANDOFF
        CB_PORT=6008
else
	CB_APP_HOME_PROJ=$CB_APP_HOME_HR
        CB_PORT=5005
fi

cd $CB_APP_HOME_PROJ
echo $CB_APP_HOME_PROJ
pwd

CMP_APPS_ARR=(600:actions:actions 601:models:models)
app=${CMP_APPS_ARR[$INDX]}
app_arr=( ${app//[:\/]/ } )
echo "CMD I/p [$1, $2, $3] APP-INFO = ${app}"

STTS=0
echo ${app_arr[2]}

case $2 in
0)
    STTS=`ps -ef|grep rasa | grep ${app_arr[2]} | grep -v grep|wc|awk '{print $1}'`
    APP_PID=`ps aux|grep rasa | grep ${app_arr[2]} | grep -v grep | awk '{print $2}'`
    ;;
*)
    STTS=`ps -ef|grep rasa | grep ${app_arr[2]} | grep ${CB_PORT} | grep -v grep|wc|awk '{print $1}'`
    APP_PID=`ps aux|grep rasa | grep ${app_arr[2]} | grep ${CB_PORT} | grep -v grep | awk '{print $2}'`
    ;;
esac

echo $STTS
case $1 in
train)
    case $2 in
    0)
        echo "Nothing to do!!!"
        ;;
    *)
        echo "rasa train --debug &"
        rasa train --debug &
        ;;
    esac
    ;;
start)
    case $STTS in
    0)
        echo "Not Running! Starting... [$3]";
        case $2 in
        0)
            if [ $3 == 'prod' ] ; then
              echo "RAN> rasa run actions >> action_server.log 2>&1 &"
              rasa run actions --debug >> $CB_LOGS_PATH/action_server.log &
            else
              echo "RAN> rasa run actions >> action_server.log 2>&1 &"
              rasa run actions --debug >> $CB_LOGS_PATH/action_server.log &
            fi
            ;;
        1)
            if [ $3 == 'prod' ] ; then
              echo "RAN> rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints-prod.yml --log-file /var/logs/chatbot/bot_hr.log &"
              rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints-prod.yml --log-file $CB_LOGS_PATH/bot_hr.log -p $CB_PORT &
            else
              echo "RAN> rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints.yml --debug --log-file /var/logs/chatbot/bot_hr.log &"
              rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints.yml --debug --log-file $CB_LOGS_PATH/bot_hr.log -p $CB_PORT &
            fi
            ;;
        2)
            if [ $3 == 'prod' ] ; then
              echo "RAN> rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints-prod.yml --log-file /var/logs/chatbot/bot_banking.log &"
              rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints-prod.yml --log-file $CB_LOGS_PATH/bot_banking.log -p $CB_PORT &
            else
              echo "RAN> rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints.yml --debug --log-file /var/logs/chatbot/bot_banking.log &"
              rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints.yml --debug --log-file $CB_LOGS_PATH/bot_banking.log -p $CB_PORT &
            fi
            ;;
        3)
            if [ $3 == 'prod' ] ; then
              echo "RAN> rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints-prod.yml --log-file /var/logs/chatbot/bot_korero-kevin.log -p $CB_PORT &"
              rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints-prod.yml --debug --log-file $CB_LOGS_PATH/bot_korero-kevin.log -p $CB_PORT &
            else
              echo "RAN> rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints.yml --debug --log-file /var/logs/chatbot/bot_korero-kevin.log -p $CB_PORT &"
              rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints.yml --debug --log-file $CB_LOGS_PATH/bot_korero-kevin.log -p $CB_PORT &
            fi
            ;;
        4)
            if [ $3 == 'prod' ] ; then
              echo "RAN> rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints-prod.yml --log-file /var/logs/chatbot/bot_poc_human_handoff.log -p $CB_PORT &"
              rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints-prod.yml --log-file $CB_LOGS_PATH/bot_poc_human_handoff.log -p $CB_PORT &
            else
              echo "RAN> rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints.yml --debug --log-file /var/logs/chatbot/bot_poc_human_handoff.log -p $CB_PORT &"
              rasa run -m models --enable-api --cors "*" --credentials credentials.yml --endpoints endpoints.yml --debug --log-file $CB_LOGS_PATH/bot_poc_human_handoff.log -p $CB_PORT &
            fi
            ;;
        esac
        echo "started"
        ;;
    1)
        echo "Already Running..."
        ;;
    *)
        echo "Multiple Instances Running...";
        ;;
    esac
    ;;
stop)
    case $STTS in
    0)
        echo "Already Stopped... [$3]"
        ;;
    1)  echo "Found Running! Stopping... [$3]";
        kill -9 $APP_PID;
        echo "stopped"
        ;;
    *)  echo "Multiple Instances Running! Stopping...  [$3]";
        kill -9 $APP_PID;
        echo "stopped"
        ;;
    esac
    ;;
update)
    echo "Updated"
    ;;
esac

exit

