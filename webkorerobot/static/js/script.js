// on input/text enter--------------------------------------------------------------------------------------
$('.usrInput').on('keyup keypress', function (e) {
	var keyCode = e.keyCode || e.which;
	var text = $(".usrInput").val();
	if (keyCode === 13) {
		if (text == "" || $.trim(text) == '') {
			e.preventDefault();
			return false;
		} else {
			$(".usrInput").blur();
			setUserResponse(text);
                        var senderid = document.getElementById('sender_id').value;
			send(text, senderid);
			e.preventDefault();
			return false;
		}
	}
});

$('.sendButton').on('click', function (e) {
	var keyCode = e.keyCode || e.which;
	var text = $(".usrInput").val();
	if (text == "" || $.trim(text) == '') {
			e.preventDefault();
			return false;
		} else {
			$(".usrInput").blur();
			setUserResponse(text);
                        var senderid = document.getElementById('sender_id').value;
			send(text, senderid);
			e.preventDefault();
			return false;
	}
});


//------------------------------------- Set user response------------------------------------
function setUserResponse(val) {


	var UserResponse = '<img class="userAvatar" src=' + "https://cdn.1ds.in/webkorerobot/static/img/userAvatar.jpg" + '><p class="userMsg">' + val + ' </p><div class="clearfix"></div>';
	$(UserResponse).appendTo('.chats').show('slow');
	$(".usrInput").val('');
	scrollToBottomOfResults();
	$('.suggestions').remove();
}

//---------------------------------- Scroll to the bottom of the chats-------------------------------
function scrollToBottomOfResults() {
	var terminalResultsDiv = document.getElementById('chats');
	terminalResultsDiv.scrollTop = terminalResultsDiv.scrollHeight;
}

function send(message, senderid) {
	console.log("User Message:", message)
	console.log("User Sender ID:", senderid)
	$.ajax({
		url: 'https://panel.koreroplatforms.com/wbkkcb/webhooks/rest/webhook',
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify({
			"message": message,
			"sender": senderid
		}),
		success: function (data, textStatus) {
			if(data != null){
			    setBotResponse(data);
                            $('.usrInput').focus();
			}
			console.log("Kevin Response: ", data, "\n Status:", textStatus)
		},
		error: function (errorMessage) {
			setBotResponse("");
			console.log('Error' + errorMessage);

		}
	});
}

//------------------------------------ Set bot response -------------------------------------
function setBotResponse(val) {
	setTimeout(function () {
		if (val.length < 1) {
			//if there is no response from Rasa
			//msg = 'I couldn\'t get that. Let\' try something else!';
			msg = 'Woof! It feels like cold winter. Please Retry Again';

			var BotResponse = '<img class="botAvatar" src="https://cdn.1ds.in/webkorerobot/static/img/botAvatar_1.png"><p class="botMsg">' + msg + '</p><div class="clearfix"></div>';
			$(BotResponse).appendTo('.chats').hide().fadeIn(1000);

		} else {
			//if we get response from Rasa
			for (i = 0; i < val.length; i++) {
				//check if there is text message
				if (val[i].hasOwnProperty("text")) {
					var BotResponse = '<img class="botAvatar" src="https://cdn.1ds.in/webkorerobot/static/img/botAvatar_1.png"><p class="botMsg">' + val[i].text + '</p><div class="clearfix"></div>';
					$(BotResponse).appendTo('.chats').hide().fadeIn(1000);
				}

				//check if there is image
				if (val[i].hasOwnProperty("image")) {
					var BotResponse = '<div class="singleCard">' +
						'<img class="imgcard" src="' + val[i].image + '">' +
						'</div><div class="clearfix">'
					$(BotResponse).appendTo('.chats').hide().fadeIn(1000);
				}

				//check if there is  button message
				if (val[i].hasOwnProperty("buttons")) {
					addSuggestion(val[i].buttons);
				}

			}
			scrollToBottomOfResults();
		}

	}, 500);
}


// ------------------------------------------ Toggle chatbot -----------------------------------------------
$('#profile_div').click(function () {
        if (document.getElementById('sender_id').value == "") {
            document.getElementById('sender_id').value == set_sndrid()
        }
	$('.profile_div').toggle();
	$('.widget').toggle();
	scrollToBottomOfResults();
        $('.usrInput').focus();
});

$('#close').click(function () {
	$('.profile_div').toggle();
	$('.widget').toggle();
});


// ------------------------------------------ Suggestions -----------------------------------------------

function addSuggestion(textToAdd) {
	setTimeout(function () {
		var suggestions = textToAdd;
		var suggLength = textToAdd.length;
		$(' <div class="singleCard"> <div class="suggestions"><div class="menu"></div></div></diV>').appendTo('.chats').hide().fadeIn(1000);
		// Loop through suggestions
		for (i = 0; i < suggLength; i++) {
			$('<div class="menuChips" data-payload=\''+(suggestions[i].payload)+'\'>' + suggestions[i].title + "</div>").appendTo(".menu");
		}
		scrollToBottomOfResults();
	}, 1000);
}


// on click of suggestions, get the value and send to rasa
$(document).on("click", ".menu .menuChips", function () {
	var text = this.innerText;
	var payload= this.getAttribute('data-payload');
        var senderid = document.getElementById('sender_id').value;
	console.log("button payload: ",this.getAttribute('data-payload'))
	setUserResponse(text);
	send(payload, senderid);
	$('.suggestions').remove(); //delete the suggestions 
});

function set_sndrid() {
  var ss = Math.floor((Math.random() * 1000) + 1);
  console.log(getJSessionId())
  var ss = /SESS\w*ID=([^;]+)/i.test(document.cookie) ? RegExp.$1 : ( (getJSessionId() != null) ? getJSessionId() : ( (get_UUID() != null) ? get_UUID() : ss ) );
  document.getElementById("sender_id").value = ss;          
}
function get_UUID(){
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
  var r = (dt + Math.random()*16)%16 | 0;
  dt = Math.floor(dt/16);
    return (c=='x' ? r :(r&0x3|0x8)).toString(16);
  });
  return uuid;
}
function getJSessionId(){
  var jsId = document.cookie.match(/JSESSIONID=[^;]+/);
  if(jsId != null) {
    if (jsId instanceof Array)
      jsId = jsId[0].substring(11);
    else
      jsId = jsId.substring(11);
  }
  return jsId;
}

