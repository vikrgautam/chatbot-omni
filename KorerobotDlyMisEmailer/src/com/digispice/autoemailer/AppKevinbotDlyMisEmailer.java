package com.digispice.autoemailer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.Base64;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
//import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


public class AppKevinbotDlyMisEmailer {
	Properties props;
	Connection con = null;
	private static final Logger log = LoggerFactory.getLogger(AppKevinbotDlyMisEmailer.class);
	
	public void loadProperties()
	{
		props = new Properties();  
		
		try {
			String PropFilePath = System.getenv("CBOT_MIS_EMAILER_PROP_PATH") + "/application.properties";
			log.debug("Trying to load properties from: " + PropFilePath);
			
			//InputStream file =this.getClass().getResourceAsStream(PropPath + "/application.properties");
			InputStream file = new FileInputStream(new File(PropFilePath));
			props.load(file);
		}
		catch (Exception e) {
			log.error("Error: " + e);
		}
		log.info("Properties loaded.");
	}
	
	public void openDbConnection()
	{
		log.info("Trying to connect PostgreSQL database " + props.getProperty("db.url"));
		try {
			con = DriverManager.getConnection(props.getProperty("db.url"), props.getProperty("db.username"), props.getProperty("db.password"));
			con.setAutoCommit(false);
			log.info("Connected to PostgreSQL database " + props.getProperty("db.url"));
		} catch (SQLException e) {
			log.error("Error while opening DB Connection... " + e);
			e.printStackTrace();
		}
	}
	
	public void closeDbConnection()
	{
		try {
			con.close();
			
		} catch (SQLException e) {
			log.error("Error: " + e);
		} finally {
			con = null;
			log.info("Connection to PostgreSQL Database Closed.");
		}
	}
	
	public ResultSet getProcResult(String callString)
	{
		ResultSet resultSet = null;
		try {
			CallableStatement stmt = null;
			stmt = con.prepareCall(callString);
			stmt.registerOutParameter(1, Types.REF_CURSOR);
			stmt.execute();
			resultSet = (ResultSet)stmt.getObject(1);
		} catch (SQLException e) {
			log.error("Error: " + e);
		}
		
		return resultSet;
	}
	
	public String getEmailBodyHtmlBase64(String date, String summaryTableHtml)
	{
		StringBuilder htmlBody = new StringBuilder();

		htmlBody.append(props.getProperty("email.salutation"));
		htmlBody.append(MessageFormat.format((String) props.getProperty("email.body"), date, props.getProperty("bot.name")));
		htmlBody.append(summaryTableHtml);
		htmlBody.append(props.getProperty("email.signature"));
		
		log.debug("Email Body: " + htmlBody.toString());
		
		String originalInput = htmlBody.toString();
		String encodedString = Base64.getEncoder().encodeToString(originalInput.getBytes());
		
		return encodedString;
	}
	
	public String getApiParamsJson(String emailContent, String recipient)
	{
		// Due to CC feature unavailabilty, we are passing recipient as param in order
		// to use this function to send email to multiple recipients.
		ObjectMapper mapper = new ObjectMapper();

		ObjectNode apiParamsJson = mapper.createObjectNode();
		apiParamsJson.put("emailContent", emailContent);
		apiParamsJson.put("password", props.getProperty("email.api.password"));
		apiParamsJson.put("receivername", "");
		apiParamsJson.put("reciever", recipient);
		apiParamsJson.put("sender", props.getProperty("email.sender"));
		apiParamsJson.put("senderName", props.getProperty("email.sendername"));
		apiParamsJson.put("subject", MessageFormat.format((String) props.getProperty("email.subject"), props.getProperty("bot.name")));
		apiParamsJson.put("userId", props.getProperty("email.api.user"));

		log.debug("apiParamsJson: " + apiParamsJson.toString());
		return apiParamsJson.toString();
	}
	
	public Attachment createAttachment(String date) {
		ResultSet resultSet = null;
		Attachment attachment = null;
		String callString = "{? = call fn_kevin_get_leads('" + date + "')}";
		try {
		    resultSet = getProcResult(callString);
		    attachment = new Attachment(resultSet, date);
		    attachment.PrepareData();
		    attachment.Create();
			resultSet.close();
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			resultSet = null;
		}
		return attachment;
	}
	
	public String getSummaryhtmldata(String month) {
		ResultSet resultSet = null;
		String callString1 = "{? = call fn_kevin_get_leads_summary(" + month + ")}";
		String summaryTableHtml = null;
		try {
			resultSet = getProcResult(callString1);
			SummaryTable leadsSummary = new SummaryTable(resultSet);
			summaryTableHtml = leadsSummary.Create();
			resultSet.close();
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			resultSet = null;
		}
		return summaryTableHtml;
	}
	
	public void SendEmail(String apiParamsJson, String attachmentFilePath)
	{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		try {
			String apiUrl = props.getProperty("email.api.url");
			HttpPost httppost = new HttpPost(apiUrl);

			FileBody fileBody = new FileBody(new File(attachmentFilePath));
			StringBody strBody = new StringBody(apiParamsJson, ContentType.APPLICATION_JSON);

			HttpEntity reqEntity = MultipartEntityBuilder.create()
					.addPart("request", strBody)
					.addPart("attachmentList", fileBody)
					.build();

			httppost.setEntity(reqEntity);

			log.debug("Executing request " + httppost.getRequestLine());
			CloseableHttpResponse response = null;
			try {
				response = httpclient.execute(httppost);
			} catch (ClientProtocolException e) {
				log.error("Error: " + e);
			} catch (IOException e) {
				log.error("Error: " + e);
			}
			try {
				log.debug(""+ response.getStatusLine());
				HttpEntity resEntity = response.getEntity();
				if (resEntity != null) {
					try {
						log.info(EntityUtils.toString(resEntity));
					} catch (ParseException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				try {
					EntityUtils.consume(resEntity);
				} catch (IOException e) {
					log.error("Error: " + e);
				}
			} finally {
				try {
					response.close();
				} catch (IOException e) {
					log.error("Error: " + e);
				}
			}
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				log.error("Error: " + e);
			}
		}	
	}
	
	public void sendDailyEmail(String date) {
		Attachment attachment = createAttachment(date);
		// Create Summary table (HTML) for Email Body.
		String [] dateParts = date.split("-");
		String month = dateParts[1];
		log.debug("Month in Date = " + month);
		String summryHtml = getSummaryhtmldata(month);
		String emailContent = getEmailBodyHtmlBase64(date, summryHtml);
		String apiParamsJson = getApiParamsJson(emailContent, props.getProperty("email.recipient"));
		
		SendEmail(apiParamsJson, attachment.getAttachmentFilePath());
		
		String otherRecipientsStr = props.getProperty("email.otherrecipients");
		if (otherRecipientsStr != null && !otherRecipientsStr.isEmpty()) {
			String[] otherRecipients = otherRecipientsStr.split(",");
		
			for (String recipient : otherRecipients){
				log.info("Sending email to other recipient: " + recipient);
				apiParamsJson = getApiParamsJson(emailContent, recipient);
				SendEmail(apiParamsJson, attachment.getAttachmentFilePath());
			}
		}
	}
	
	public static void main(String[] args) throws ParseException, IOException {
		AppKevinbotDlyMisEmailer appObj = new AppKevinbotDlyMisEmailer();
		
		String date = args[0];

		appObj.loadProperties();
		appObj.openDbConnection();
		appObj.sendDailyEmail(date);
		appObj.closeDbConnection();
		
		log.info("Auto email for " + date +  " sent successfully. Exiting ...");
	}
}
