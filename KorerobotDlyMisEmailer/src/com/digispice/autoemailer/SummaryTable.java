package com.digispice.autoemailer;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SummaryTable {
	private static final Logger log = LoggerFactory.getLogger(SummaryTable.class);
	ResultSet resultSet;

	public SummaryTable(ResultSet resultSet) {
		super();
		this.resultSet = resultSet;
	}
	
	public String Create() {
		
		StringBuilder htmlBuilder = new StringBuilder();
		
		try {
               	
        	htmlBuilder.append("<table BORDER=1 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>");
        	htmlBuilder.append("<tr>"
        			+ "<th>Date</th>"
        			+ "<th>Total Interactions</th>"
        			+ "<th>Completed Interactions</th>"
        			+ "<th>CPAAS Interactions</th>"
        			+ "<th>MA Interactions</th>"
        			+ "<th>Others</th>"
        			+ "</tr>");
        	
			while(resultSet.next())
			{
				log.debug("Date: " + resultSet.getString(1) + " Total Interactions: " + resultSet.getString(2) + " Completed Interactions: " + resultSet.getString(3) +
						" CPAAS Interactions: " + resultSet.getString(4) + " MA Interactions: " + resultSet.getString(5) + " Others: " + resultSet.getString(6));
				
				htmlBuilder.append(String.format("<tr>"
						+ "<td><center>%s</center></td>"
						+ "<td><center>%s</center></td>"
						+ "<td><center>%s</center></td>"
						+ "<td><center>%s</center></td>"
						+ "<td><center>%s</center></td>"
						+ "<td><center>%s</center></td>"
						+ "</tr>",
						resultSet.getString(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6)));
			}
			
        	htmlBuilder.append("</table>");
		}
        catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return htmlBuilder.toString();
	}
}
