package com.digispice.autoemailer;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Attachment {
	private static final Logger log = LoggerFactory.getLogger(Attachment.class);
	ResultSet resultSet;
	Map<Integer,Object[]> data;
	String date;
	String attachmentFilePath;

	public String getAttachmentFilePath() {
		return attachmentFilePath;
	}

	public void setAttachmentFilePath(String attachmentFilePath) {
		this.attachmentFilePath = attachmentFilePath;
	}

	public Attachment(ResultSet resultSet, String date) {
		super();
		this.resultSet = resultSet;
		this.date = date;
	}
	
	public void PrepareData() {
		
		int num = 1;
        
        data = new TreeMap<Integer, Object[]>();
        data.put(num, new Object[]{ date });
        num++;
        data.put(num, new Object[]{ "S.No.", "Customer Name", "Email ID", "Mobile Number", "OTP Verified", "Selected Offering", "Sub Offering", "Time of Interaction" });
        
        try {
			while(resultSet.next())
			{
				num++;
				            	
				log.debug("Customer Name: " + resultSet.getString(10) + " Email ID: " + resultSet.getString(11) + " Mobile Number: " + resultSet.getString(12) +
						" OTP Verified: " + resultSet.getString(15) + " Selected Offering: " + resultSet.getString(16) + " Sub Offering: " +
						resultSet.getString(17) + " Time of Interaction: " + resultSet.getString(3));
				
				data.put(num, new Object[]{ num-2, resultSet.getString(10), resultSet.getString(11), resultSet.getString(12),
						resultSet.getString(15), resultSet.getString(16), resultSet.getString(17), resultSet.getString(3)});
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void Create() {
		@SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook();
	      
        // Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("kevin_visitors_data");
  
        // Iterate over data and write to sheet
        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for (Integer key : keyset) {
            // this creates a new row in the sheet
            XSSFRow row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row
                XSSFCell cell = row.createCell(cellnum++);
                XSSFCellStyle style = workbook.createCellStyle();
                
                style.setBorderBottom(BorderStyle.THIN);
                style.setBorderTop(BorderStyle.THIN); 
                style.setBorderRight(BorderStyle.THIN); 
                style.setBorderLeft(BorderStyle.THIN); 
                
                // Make Header row bold.
                if (row.getRowNum() == 1) {
                	
                    XSSFFont font = workbook.createFont();
                    font.setBold(true);
                    style.setFont(font);                   
                }
                
            	cell.setCellStyle(style);
                
                if (obj instanceof String)
                    cell.setCellValue((String)obj);
                else if (obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        try {
        	File attachmentFile = new File("auto-emailer-attachment.xlsx");
        	attachmentFilePath = attachmentFile.getAbsolutePath();
            FileOutputStream out = new FileOutputStream(attachmentFile);
            workbook.write(out);
            out.close();
            
            log.debug(attachmentFilePath + " written successfully on disk.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
	}
}
