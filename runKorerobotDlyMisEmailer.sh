#!/bin/bash
source ~/.bash_profile
pwd
#DATE=$(date +'%Y-%m-%d')
echo -e CBOT_MIS_EMAILER_JAR_PATH = $CBOT_MIS_EMAILER_JAR_PATH
echo -e CBOT_MIS_EMAILER_PROP_PATH = $CBOT_MIS_EMAILER_PROP_PATH

PREV_DATE=$(date --date="1 days ago" +'%Y-%m-%d')

nohup java -jar $CBOT_MIS_EMAILER_JAR_PATH/kbotDlyMisEmailer.jar $PREV_DATE  & >/dev/null 2>&1

echo -e "$CBOT_MIS_EMAILER_JAR_PATH/kbotDlyMisEmailer.jar started with date $PREV_DATE.";
